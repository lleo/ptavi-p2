#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo

class CalcChild (calcoo.Calc):

    def mul(self):

         return (self.op1) * (self.op2)

    def div(self):

        if self.op2 == 0:
            raise ValueError("Division by zero is not allowed")
        else:
            return self.op1 / self.op2

    def operar(self):

        if self.operador == "x":
            return self.mul()
        elif self.operador == "/":
            return self.div()
        else:
            return calcoo.Calc.operar(self)

if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("$ python3 calcoochild.py operando1 operación operando2")

    operador = sys.argv[2]
    op1 = sys.argv[1]
    op2 = sys.argv[3]
    try:
        op1 = float(op1)
        op2 = float(op2)
    except ValueError:
        sys.exit("Los operandos deben ser números")

    objeto = CalcChild(operador, op1, op2)
    print(objeto.operar())
