import sys
from functools import reduce
import csv

from calccount import Calc

calc = Calc()


def process_csv(file):
    with open(file, 'r') as reader:
        lines = csv.reader(reader, delimiter=",")
        for line in lines:
            oper = line[0]
            args = line[1:]
            if oper == "+":
                reduce(calc.add, args)
            elif oper == "-":
                reduce(calc.sub, args)
            elif oper == "*":
                reduce(calc.mul, args)
            elif oper == "/":
                reduce(calc.div, args)
            else:
                print(f"Operador no encontrado, solo se permiten los siguientes operadores: +, - , *, /")


if __name__ == '__main__':
    process_csv(sys.argv[1])