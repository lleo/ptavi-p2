#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Calc:

    def __init__(self):
        self.count = 0

    def add(self, x, y):
        self.count += 1
        try:
            r = float(int(x) + int(y))
            print(r)
            return r
        except Exception as e:

            print("Bad format")

    def sub(self, x, y):

        self.count += 1
        try:
            r = float(int(x) - int(y))
            print(r)
            return r
        except Exception as e:
            print("Bad format")

    def mul(self, x, y):
        self.count += 1
        try:
            r = float(int(x) * int(y))
            print(r)
            return r
        except Exception as e:
            print("Bad format")

    def div(self, x, y):
        try:
            self.count += 1
            r = float(x / y)
            print(r)
            return r
        except Exception as e:
            print("Division by zero is not allowed")
