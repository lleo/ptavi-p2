#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from functools import reduce

from calccount import Calc

calc = Calc()


def process_csv(file):
    with open(file, 'r') as reader:
        lines = reader.readlines()
        for line in lines:
            operators = line.replace("\n", "").split(",")
            oper = operators[0]
            args = operators[1:]
            if oper == "+":
                reduce(calc.add, args)
            elif oper == "-":
                reduce(calc.sub, args)
            elif oper == "*":
                reduce(calc.mul, args)
            elif oper == "/":
                reduce(calc.div, args)
            else:
                print(f"Operador no encontrado, solo se permiten los siguientes operadores: +, - , *, /")


if __name__ == '__main__':
    process_csv(sys.argv[1])


